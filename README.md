# basics

this is an example docker image used actix_web [basic http server](https://github.com/actix/examples/tree/master/basics)
## Usage

### server

```bash
cargo run
# Started http server: 127.0.0.1:8085
```

### web client

- [http://localhost:8085/](http://localhost:8085/static/index.html)
- [http://localhost:8085/async-body/bob](http://localhost:8085/async-body/bob)
- [http://localhost:8085/user/bob/](http://localhost:8085/user/bob/) text/plain download
- [http://localhost:8085/test](http://localhost:8085/test) (return status switch GET or POST or other)
- [http://localhost:8085/favicon](http://localhost:8085/static/favicon.htmicol)
- [http://localhost:8085/welcome](http://localhost:8085/static/welcome.html)
- [http://localhost:8085/notexit](http://localhost:8085/static/404.html) display 404 page
- [http://localhost:8085/error](http://localhost:8085/error) Panic after request 
