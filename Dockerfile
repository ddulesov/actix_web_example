FROM alpine:3.7 
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
     && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.30-r0/glibc-2.30-r0.apk \
     && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.30-r0/glibc-bin-2.30-r0.apk \
     && apk add glibc-2.30-r0.apk glibc-bin-2.30-r0.apk \
     && rm glibc-2.30-r0.apk glibc-bin-2.30-r0.apk
WORKDIR /usr
COPY ./static .
COPY ./target/release/basics  ./bin/basics
EXPOSE 8085
CMD [ "bin/basics" ]
